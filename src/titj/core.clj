(ns titj.core
  (:require [compojure.core :refer [defroutes GET]]
            [ring.adapter.jetty :as ring]
            [hiccup.page :as page]))

(defn index []
  (page/html5
   [:head
    [:title "A Thorough Introduction"]]
   [:body

    [:h1 {:class "header"} "A Thorough Introduction"
     [:small "Language of Your Choice"]]

    [:div {:id "parent-wrapper"}
      [:div {:id "parent"} ]]

    [:div {:id "main"}]

    (include-js "js/utils.js")
    (include-js "js/components/header.js")
    (include-js "js/components/solarsystem.js")
    (include-js "js/components/drawing.js")
    (include-js "js/components/playground.js")
    (include-js "js/components/pingpong.js")
    (include-js "js/main.js")]))


(defroutes routes
  (GET "/" [] (index)))

(defn -main []
  (ring/run-jetty #'routes {:port 8080 :join? false}))

